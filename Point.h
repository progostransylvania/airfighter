/*
 * Point.h
 *
 *  Created on: Oct 8, 2018
 *      Author: kolczagy
 */

#ifndef POINT_H_
#define POINT_H_

class Point {
public:
	Point();
	Point(int xval, int yval);
	int GetX();
	int GetY();
	virtual ~Point();

private:
	int x, y;
};


#endif /* POINT_H_ */
