//============================================================================
// Name        : competition.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <sstream>
#include "AirFighter.h"
#include "base64.h"
using namespace std;


int** parseMap(string const& map_string) {
	int** map = new int*[50];

	std::string line;
	std::istringstream f(map_string);
	string intermediate;
	int row = 0;
	while (std::getline(f, line)) {
		map[row] = new int[50];

		std::string::size_type lastPos = line.find_first_not_of(",", 0);
		std::string::size_type pos = line.find_first_of(",", lastPos);

		int col = 0;
		while (std::string::npos != pos || std::string::npos != lastPos) {
			string element = line.substr(lastPos, pos - lastPos);
			map[row][col] = stoi(element);
			lastPos = line.find_first_not_of(",", pos);
			pos = line.find_first_of(",", lastPos);
			col++;
		}
		row++;
	}

	return map;
}

int main(int argc, char *argv[]) {
	AirFighter a;

	string mapString = base64_decode(argv[1]);
	int** map = parseMap(mapString);
	Point p = a.hit(map, 50, 50);
	cout << p.getX() << "," << p.getY();
	return 0;
}

