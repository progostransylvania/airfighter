#include <iostream>

#include <iostream>
#include <sstream>
#include "AirFighter.h"
#include "base64.h"
using namespace std;

const int n=50;
const int m=50;

int** parseMap(string const& map_string) {
	int** map = new int*[n];

	std::string line;
	std::istringstream f(map_string);

    int row = 0;
	while (std::getline(f, line)) {
		map[row] = new int[m];

		std::string::size_type lastPos = line.find_first_not_of(",", 0);
		std::string::size_type pos = line.find_first_of(",", lastPos);

		int col = 0;
		while (std::string::npos != pos || std::string::npos != lastPos) {
			string element = line.substr(lastPos, pos - lastPos);
			map[row][col] = stoi(element);
			lastPos = line.find_first_not_of(",", pos);
			pos = line.find_first_of(",", lastPos);
			col++;
		}
		row++;
	}

	return map;
}

int main(int argc, char *argv[]) {

    if(argc != 2) {
      cout << "You need to supply one argument to this program.";
      return -1;
    }

	AirFighter a;

	string mapString = base64_decode(argv[1]);
	int** map = parseMap(mapString);
	Point p = a.Hit(map, n, m);
	cout << p.GetX() << "," << p.GetY();
	return 0;
}

