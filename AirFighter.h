/*
 * AirFighter.h
 *
 *  Created on: Oct 8, 2018
 *      Author: kolczagy
 */

#ifndef AIRFIGHTER_H_
#define AIRFIGHTER_H_
#include "Point.h"

class AirFighter {
public:
	AirFighter();
	Point Hit(int** map, int row, int col);
	void PrintMap(int** map, int row, int col);
	virtual ~AirFighter();
};

#endif /* AIRFIGHTER_H_ */
