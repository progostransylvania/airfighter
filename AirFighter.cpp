/*
 * AirFighter.cpp
 *
 *  Created on: Oct 8, 2018
 *      Author: kolczagy
 */

#include "AirFighter.h"
#include <stddef.h>
#include <iostream>
#include <ctime>
using namespace std;

AirFighter::AirFighter() {
	// TODO Auto-generated constructor stub
}

Point AirFighter::Hit(int** map, int row, int col) {

	//printMap(map, row, col);
	srand(time(NULL));
	int x = std::rand() % row;
	int y = std::rand() % col;

	return Point(x, y);
}

AirFighter::~AirFighter() {
	// TODO Auto-generated destructor stub
}

void AirFighter::PrintMap(int** map, int row, int col) {
	for (int i = 0; i < row; ++i) {
		for (int j = 0; j < col; ++j) {
			std::cout << map[i][j] << ",";
		}
		std::cout << "\n";
	}
}

