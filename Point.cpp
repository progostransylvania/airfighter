/*
 * Point.cpp
 *
 *  Created on: Oct 8, 2018
 *      Author: kolczagy
 */

#include "Point.h"

Point::Point(int xval, int yval) {
	x = xval;
	y = yval;
}

Point::~Point() {
	// TODO Auto-generated destructor stub
}

int Point::GetX(){
	return x;
}

int Point::GetY(){
	return y;
}

